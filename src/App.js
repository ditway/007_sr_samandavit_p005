import { useState } from "react";
import {
  Badge,
  Card,
  Button,
  Row,
  Col,
  Container,
  Table,
} from "react-bootstrap";
import "./App.css";
const products = [
  {
    id: 1,
    name: "Pizza",
    price: 15,
    img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxh-YtNAClJ0rt4tuYWVkrg8BmlA_lMU1Tjw&usqp=CAU",
  },
  {
    id: 2,
    name: "Chicken",
    price: 5,
    img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOo91EQJFM7kXVu0smuiCcflZpzmxXFQJsTQ&usqp=CAU",
  },
  {
    id: 3,
    name: "Burger",
    price: 6,
    img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOl9QbWWFwwEJR_BUNNh4BIABv8eogsi8maA&usqp=CAU",
  },
  {
    id: 4,
    name: "Coca",
    price: 2,
    img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSXwpwC4Sd8t6c_pprsPg4Bikq_1oV_oAlXBg&usqp=CAU",
  },
];
function CardProduct(props) {
  const { product, count, onAdd, onDelete, total } = props;
  return (
    <Card className="card">
      <Card.Img
        style={{ height: 200 }}
        variant="top"
        src={product.img}
        alt={product.name}
      />
      <Card.Body>
        <h6>{product.name}</h6>
        <p className="text-secondary">Price: {product.price} $</p>
        <div className="d-flex align-items-center mb-2">
          <Badge variant="warning">{count}</Badge>
          <Button
            variant="primary"
            className="ml-2"
            onClick={() => onAdd(product)}
          >
            Add
          </Button>

          <Button
            variant="danger"
            className="ml-2"
            onClick={() => onDelete(product)}
          >
            Delete
          </Button>
        </div>
        <h6 className="m-0">Total: {total} $</h6>
      </Card.Body>
    </Card>
  );
}
function App() {
  const [orders, setOrder] = useState([]);
  function addOrder(item) {
    const order = orders.find((x) => x.id === item.id);
    if (order) {
      order.qty += 1;
    } else {
      const newOrder = {
        id: item.id,
        name: item.name,
        qty: 1,
        price: item.price,
        total: () => {
          return newOrder.qty * newOrder.price;
        },
      };
      orders.push(newOrder);
    }
    setOrder([...orders]);
  }
  function deleteOrder(item) {
    const order = orders.find((x) => x.id === item.id);
    if (order) {
      if (order.qty > 0) order.qty -= 1;
      if (order.qty === 0) {
        setOrder([...orders.filter((x) => x.id !== item.id)]);
      } else setOrder([...orders]);
    }
  }
  function getOrderCountByProduct(id) {
    return orders.find((x) => x.id === id)?.qty || 0;
  }
  function getTotalOrderPriceByProduct(id) {
    return orders.find((x) => x.id === id)?.total() || 0;
  }
  function renderCard(item, index) {
    return (
      <Col key={index} className="mb-4 col-lg-3 col-md-5 col-sm-6">
        <CardProduct
          product={item}
          key={index}
          onAdd={addOrder}
          onDelete={deleteOrder}
          count={getOrderCountByProduct(item.id)}
          total={getTotalOrderPriceByProduct(item.id)}
        />
      </Col>
    );
  }
  function resetOrder() {
    setOrder([]);
  }
  function getCountOrder() {
    return orders.length;
  }
  function renderTable() {
    return (
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Qty</th>
            <th>Unit Price</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          {orders.map((item, index) => {
            return (
              <tr key={index}>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.qty}</td>
                <td>{item.price} $</td>
                <td>{item.total()} $</td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    );
  }
  return (
    <div>
      <div className="p-3 d-flex justify-content-between">
        <h5 className="h6">Navbar with text</h5>
        <h6 className="text-secondary">
          Signed in as: <a href="#">Sam an davit</a>
        </h6>
      </div>
      <div className="p-3">
        <h2 className="text-center">FAST FOOT</h2>
      </div>
      <Container className="p-3">
        <Row className="flex-wrap justify-content-center">
          {products.map(renderCard)}
        </Row>
        <div className="d-flex mb-3 align-items-center">
          <Button variant="info" onClick={resetOrder}>
            Reset
          </Button>
          <Badge className="ml-2" variant="warning">
            {getCountOrder()} count
          </Badge>
        </div>
        {renderTable()}
      </Container>
    </div>
  );
}

export default App;
